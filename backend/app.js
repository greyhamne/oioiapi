// TODO all requests should have catches

//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
const path = require("path")
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//  ============================================================================================================
//  Constants
//  ============================================================================================================
const {DB_USERNAME, DB_PASSWORD, DB_NAME} = require('./constants/constant-db');

const app = express();

//  ============================================================================================================
//  Connect to our mongodb cluster
//  ============================================================================================================
mongoose.connect("mongodb+srv://" + DB_USERNAME + ":"+ DB_PASSWORD  +"@cluster0-njo2n.mongodb.net/" + DB_NAME + "?retryWrites=true", 
    {useNewParser: true})
    .then(() => {
        console.log('Connected to database');
    })
    .catch(() => {
        console.log('Failed to conect');
    });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/public', express.static(path.join("backend/public")));

//  ============================================================================================================
//  Allowing CORS
//  ============================================================================================================
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS, PUT");
    next();
});

//  ============================================================================================================
//  Accessing Routes
//  ============================================================================================================
app.use("/api/listing", require("./routes/route-listing"))
app.use("/api/listings", require("./routes/route-listings"))
app.use("/api/marketplace", require("./routes/route-marketplace"))
app.use("/api/bids", require("./routes/route-marketplace-bids"))
app.use("/api/users", require("./routes/route-users"))

//  ============================================================================================================
//  Global Export shipped with all middleware
//  ============================================================================================================
module.exports = app;