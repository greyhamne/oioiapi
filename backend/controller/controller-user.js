const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const fs = require('fs');
const mv = require('mv');
const mkdirp = require('mkdirp');

const User = require('../models/model-users')

const {SECRET} = require('../constants/constant-general');

//  ============================================================================================================
//  POST a newly registered user
//  ============================================================================================================

exports.registerUser = (req, res, next) => {

    bcrypt.hash(req.body.password, 10)
    .then(hash => {
        const user = new User({
            email: req.body.email,
            password: hash,
            username: req.body.username
        });
 
        user.save()
        .then(result => {
            res.status(201).json({
                message: "User created",
                result: result,
            });
        })
    });
}

//  ============================================================================================================
//  POST - user login
//  ============================================================================================================

exports.loginUser = (req, res, next) => {

    console.log(req.params)

    let fetchedUser;
    User.findOne({
        email: req.body.email
    })
    .then(user => {
        if(!user) {
            return res.status(401).json({
                message: "No user exists with attempted email"
            })
        }
        fetchedUser = user;
        return bcrypt.compare(req.body.password, user.password)
    })
    .then(result => {
        if(!result) {
            return res.status(401).json({
                message: "Email/Password combination incorrect"
            });
        }

        const token = jwt.sign({
            email: fetchedUser.email,
            userId: fetchedUser._id,
        }, 
        SECRET,
        { expiresIn: '24h' }
        );

        res.status(200).json({
            token,
            userId: fetchedUser._id,
            email: fetchedUser.email,
            cover: fetchedUser.cover,
            profile: fetchedUser.profile,
            firstname: fetchedUser.firstname,
            secondname: fetchedUser.lastname,
        })
    })
    .catch(err => {
        console.log(err)

        return res.status(401).json({
            message: "Authentication failed"
        })
    })
}

exports.checkUsername =  (req, res, next) => {

    User.findOne({
        username: req.params.username
    }).then(data => {
        if(data) {
            res.status(200).json({
                message: "Username is already taken",
                isUsernameTaken: true
            });
        } else {
            res.status(200).json({
                message: "Username is available",
                isUsernameTaken: false
            });
        }
    }).catch(err => {
        return res.status(401).json({
            message: "Failed to check username status"
        })
    })
}

exports.checkEmail = (req, res, next) => {

    User.findOne({
        email: req.params.email
    }).then(data => {
        console.log(data)
        if(data) {
            res.status(200).json({
                message: "Email is already taken",
                isEmailTaken: true
            });
        } else {
            res.status(200).json({
                message: "Email is available",
                isUsernameTaken: false
            });
        }
    }).catch(err => {
        return res.status(401).json({
            message: "Failed to check email status"
        })
    })
}

exports.addCover = (req, res, next) => {

    const url = req.protocol + '://' + req.get("host");

    // Checks to see if a folder already exists, if not adds one
    if (!fs.existsSync('backend/public/' + req.params.id + '/cover')){
        fs.mkdirSync('backend/public/' + req.params.id + '/cover');
    }

    // move our newly created file
    mv('uploads/' + req.file.filename, 
       'backend/public/' + req.params.id + '/cover/' + req.file.filename, 
       function(err) {
        
    });

    const user = new User({
        _id: req.params.id,
        cover: url + '/public/' + req.params.id + '/cover/' + req.file.filename
    });

    User.updateOne({_id: req.params.id}, user).then(result => {
        
        res.status(200).json({
            message: 'Cover photo addded'
        })
    })
}

exports.addProfile = (req, res, next) => {

    const url = req.protocol + '://' + req.get("host");

    console.log(req.params)

    // Checks to see if a folder already exists, if not adds one
    if (!fs.existsSync('backend/public/' + req.params.id + '/profile')){
        fs.mkdirSync('backend/public/' + req.params.id + '/profile');
    }

    // // move our newly created file
    mv('uploads/' + req.file.filename, 
       'backend/public/' + req.params.id + '/profile/' + req.file.originalname, 
       function(err) {
     
    });

    const user = new User({
        _id: req.params.id,
        profile: url + '/public/' + req.params.id + '/profile/' + req.file.originalname
    });

    User.updateOne({_id: req.params.id}, user).then(result => {
        
        res.status(200).json({
            message: 'Profile photo addded'
        })
    }).catch(err => {
        res.status(500).json({
            error: err,
            errorCode: 500
        })
    });
}

exports.updateDetails = (req, res, next) => {

    const updateObject = req.body; // body of info passed
    const id = req.params.id;

    User.update({_id: id}, {$set: updateObject}).then(result => {
        res.status(200).json({
            message: 'Users Details updated'
        })
    }).catch(err => {
        res.status(500).json({
            error: err,
            errorCode: 500
        })
    });
}

