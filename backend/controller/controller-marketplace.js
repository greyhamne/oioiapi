//  ============================================================================================================
//  listings - all logic for getting a users listings
//  ============================================================================================================

const Listing = require('../models/model-listings');
const User = require('../models/model-users');
const Bidding = require('../models/model-marketplace-bids')

exports.viewListing = (req, res, next) => {
    

    Bidding.find({itemId: req.params.id})
        .then(data => {
            console.log(data)
        })
        .catch(err => {
            console.log(err)
        })

    Listing.findById(req.params.id).then(data => {
        if (data) {
            User.findOneAndUpdate({
                email: data.userId
            })
            .then((userData) => {
                console.log(userData)
                res.status(200).json({
                    ...data,
                    sellerId: userData._id,
                    sellerUsername: userData.username,
                    sellerCover: userData.cover
                });
            })
        } else {
            res.status(404).json({ message: "Listings not found!" });
        }
    });
}

exports.viewListings = (req, res, next) => {
    Listing.find({ "userId": { "$ne": req.params.userId }}).then(data => {

        if (data) {
            res.status(200).json(data);
        } else {
            res.status(404).json({ message: "Listings not found!" });
        }
    });
}
