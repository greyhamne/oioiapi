//  ============================================================================================================
//  listing - all logic for single listings ie CRUD operation
//  ============================================================================================================
const fs = require('fs')
const path = require('path')
const mv = require('mv');
const Listing = require('../models/model-listings');
const Bidding = require('../models/model-marketplace-bids')
const {ROOT} = require('../constants/constant-general')
const timestamps = require('mongoose-unix-timestamp-plugin');
const moment = require('moment')

const mkdirSync = function (dirPath) {
    try {
      fs.mkdirSync(dirPath)
    } catch (err) {
      if (err.code !== 'EEXIST') throw err
    }
  }

exports.createDraft = (req, res, next) => {

    const listing = new Listing({
        userId: req.params.userId,
        isDraft: true
    });

    listing.save().then(result => {
        mkdirSync(path.resolve(`backend/public/${req.params.userId}`))
        mkdirSync(path.resolve(`backend/public/${req.params.userId}/listings`))
        mkdirSync(path.resolve(`backend/public/${req.params.userId}/listings/${result._id}`))

        res.status(201).json({
            message: "Draft created",
            listingId: result._id
        })
    }).catch(err => {
        res.status(500).json({
            error: err,
            errorCode: 500
        })
    });
}

exports.createListing = (req, res, next) => {

    let auctionStartDate = ''

    if (req.body.auctionStartDate === '') {
        auctionStartDate = moment().unix()
    } else {
        auctionStartDate = req.params.auctionStartDate
    }

    const listing = new Listing({
        userId: req.body.userId,
        title: req.body.title,
        description: req.body.description,
        method: req.body.method,
        delivery: req.body.delivery,
        cost: req.body.cost,
        postage: req.body.postage,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        auctionHighestBid: req.body.auctionStartPrice,
        auctionCurrentBid: req.body.auctionStartPrice,
        auctionStartDate: auctionStartDate,
        auctionStartTime: req.body.auctionStartTime,
        isDraft: false
    });

    listing.save().then(result => {

        // Listing was made now store images 
        mkdirSync(path.resolve(`backend/public/${req.body.userId}`))
        mkdirSync(path.resolve(`backend/public/${req.body.userId}/listings`))
        mkdirSync(path.resolve(`backend/public/${req.body.userId}/listings/${result._id}`))

        const uploadedFiles = []

        /////////////////
        req.files.forEach(file => {
            console.log(file.originalname)
    
            fs.rename(`uploads/${file.filename}`,  `backend/public/${req.body.userId}/listings/${result._id}/${file.originalname}`, (err) => {
            if (err) return err;
                console.log('Rename complete!');
            });

            uploadedFiles.push(
                `${ROOT}/public/${req.body.userId}/listings/${result._id}/${file.originalname}`
            )
        });

        updatedDetails = {
            images: uploadedFiles
        }

        Listing.findOneAndUpdate({_id: result._id}, { $set: updatedDetails }, { upsert: true, new: true })
        .then(result => {

            console.log('Updating with image paths');

            if(req.body.method === 'Auction') {

                console.log('Auction detected');

                const bidding = new Bidding({
                    itemId: result._id,
                })
    
                bidding.save()
                .then((res1) => {
                    res.status(201).json({
                        result
                    })
                })
                .catch((err1) => {
                    res1.status(500).json({
                        error: err1,
                        errorCode: 500
                    })
                });
            } else {
                console.log('No auction detected');
                res.status(201).json({
                    result
                })
            }

            
        })
        .catch(err => {
            res.status(500).json({
                error: err,
                errorCode: 500
            })
        })
        
    }).catch(err => {
        res.status(500).json({
            error: err,
            errorCode: 500
        })
    });
}

exports.editListing = (req, res, next) => {


    if ( typeof req.files !== "undefined" ){
        const uploadedFiles = []

        req.files.forEach(file => {
            console.log(file.originalname)
    
            fs.rename(`uploads/${file.filename}`,  `backend/public/${req.body.id}/listings/${req.params.listingId}/${file.originalname}`, (err) => {
            if (err) return err;
                console.log('Rename complete!');
            });

            uploadedFiles.push(
                `${ROOT}/public/${req.body.id}/listings/${req.params.listingId}/${file.originalname}`
            )
        });

        updatedDetails = {
            title: req.body.title,
            description: req.body.description,
            userId: req.body.id,
            images: uploadedFiles
        }
    } else {
        updatedDetails = {
            ...req.body,
            isDraft: false
        }
    }

    Listing.findOneAndUpdate({_id: req.params.listingId}, { $set: updatedDetails }, { upsert: true, new: true })
    .then(result => {
        res.status(201).json({
            result
        })
    })
    .catch(err => {
        res.status(500).json({
            error: err,
            errorCode: 500
        })
    })
}

exports.getListing = (req, res, next) => {
    Listing.findById(req.params.id).then(data => {
        if (data) {
            res.status(200).json(data);
        } else {
            res.status(404).json({ message: "Listings not found!" });
        }
    }).catch(err => {
        res.status(500).json({
            error: err,
            errorCode: 500
        })
    });
}

exports.deleteListing = (req, res, next) => {
    Listing.findByIdAndRemove(req.params.id)
    .then(result =>{
        res.status(200).json({message: "Listing Deleted"});
    }).catch(err => {
        res.status(500).json({
            error: err,
            errorCode: 500
        })
    });
}
