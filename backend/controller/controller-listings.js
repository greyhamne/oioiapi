//  ============================================================================================================
//  listings - all logic for getting a users listings
//  ============================================================================================================

const Listing = require('../models/model-listings');

exports.getListings = (req, res, next) => {
    console.log("Request: Get all of my listings")
    
    Listing.find({isDraft: false}).then(data => {
        if (data) {
            res.status(200).json(data);
        } else {
            res.status(404).json({ message: "Listings not found!" });
        }
    });
}
