//  ============================================================================================================
//  listings - all logic for getting a users listings
//  ============================================================================================================

const Listing = require('../models/model-listings');
const Bidding = require('../models/model-marketplace-bids')

exports.placeBid = (req, res, next) => {
    console.log('INCOMING BID')

    // 1. Find the listing
    // 2. check if the bid coming in is higher than the max
    // 3. If the bid is not higher 
        // a. add bid to current history of bid
        // b. feedback to user the bid was not the highest
        // c. the winning bid needs to match that of the users bid

    // 3. If the user is the winning bidder
        // a. add bid to current history of bids
        // b. change the winning userId in listing to that of the bidder
        // c. update highest bid with what the user submited
        // d. update winning bid to 1p plus the previous current bid

    
    Listing.findOne({_id: req.params.itemId})
    .then(result => {
        console.log(`item id ${result._id}`)

        console.log(`Highest Bid: ${result.auctionHighestBid}`)
        console.log(`Incoming bid: ${req.body.bid}`)

        // Store a users bid 
        Bidding.findOneAndUpdate({itemId: req.params.itemId}, { $push: { 
            bids: {
                    "userId" : req.body.userId,
                    "bid" : req.body.bid
                }  
            } 
        })
        .then(result => {
            console.log("Bid added to history")
        })
        .catch(err => {
            console.log("Error updating bids history")
        })

        if(req.body.bid > result.auctionHighestBid) {
            console.log("Bid was higher we have a new winner")

            responseMsg = "Congratulations you have the highest bid"

            const newCurrentBid = parseFloat(result.auctionCurrentBid) + parseFloat(0.01)

            console.log(`New winning bid ${newCurrentBid}`)

            updateAuctionStatus = {
                winningBidder: req.body.userId,
                auctionHighestBid: req.body.bid,
                auctionCurrentBid: newCurrentBid.toFixed(2)
            }
        } 
        
        if(req.body.bid <= result.auctionHighestBid) {
            console.log("Bid was not the highest")
            
            responseMsg = 'You are not the highest bidder'

            console.log(`New winning bid ${newCurrentBid}`)

            updateAuctionStatus = {
                auctionCurrentBid: newCurrentBid.toFixed(2)
            }
        }

        Listing.findOneAndUpdate({_id: req.params.itemId}, { $set: updateAuctionStatus }, { upsert: true, new: true })
        .then(result1 => {
            console.log(updateAuctionStatus)
            console.log(result1._id)

            res.status(201).json({
                msg: responseMsg,
                result1
            })
        })
        .catch(err1 => {
            res1.status(500).json({
                error: "err1",
                errorCode: 500
            })
        })        
    })
    .catch(err => {
        res.status(500).json({
            error: "err",
            errorCode: 500
        })
    })
    
}

