//  ============================================================================================================
//  Marketplace routes
//  ============================================================================================================
const express = require("express");
const CheckAuth = require('../middleware/middleware-auth')
const MarketplaceBidsController = require("../controller/controller-marketplace-bids")
const router = express.Router()

// Should we create an empty bids history???
router.post('/:itemId', CheckAuth, MarketplaceBidsController.placeBid)

module.exports = router;