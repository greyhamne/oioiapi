//  ============================================================================================================
//  users - all logic for registering, loggin in and updating details
//  ============================================================================================================
const express = require("express");
const multer  = require('multer')
const UserController = require("../controller/controller-user")
const {STORAGE} = require('../constants/constant-general');
const router = express.Router()

//  ============================================================================================================
//  Routes with controllers
//  ============================================================================================================

const upload = multer({ dest: 'uploads/' })

router.post("/register", UserController.registerUser)
router.post("/login", UserController.loginUser)
router.post("/:username", UserController.checkUsername)
router.post("/email/:email", UserController.checkEmail)

router.patch("/cover/:id", 
    upload.single("cover"),
    UserController.addCover
)

router.patch("/profile/:id", 
    upload.single("profile"),
    UserController.addProfile
)

router.patch("/:id", UserController.updateDetails)

module.exports = router;