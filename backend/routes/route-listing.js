//  ============================================================================================================
//  Listing routes
//  ============================================================================================================

const express = require("express");
const multer  = require('multer')
const CheckAuth = require('../middleware/middleware-auth')
const ListingController = require("../controller/controller-listing")
const router = express.Router()

const upload = multer({ dest: 'uploads/' })

router.post('/draft/:userId', CheckAuth, ListingController.createDraft)
router.post('', CheckAuth,upload.any(), ListingController.createListing)
router.patch("/:listingId", CheckAuth, upload.any(), ListingController.editListing)
router.get('/:id', CheckAuth, ListingController.getListing)
router.delete('/:id', CheckAuth, ListingController.deleteListing)

module.exports = router;
