//  ============================================================================================================
//  Marketplace routes
//  ============================================================================================================
const express = require("express");
const CheckAuth = require('../middleware/middleware-auth')
const MarketplaceController = require("../controller/controller-marketplace")
const router = express.Router()

router.get('/:userId', CheckAuth, MarketplaceController.viewListings)
router.get('/listing/:id', CheckAuth, MarketplaceController.viewListing)


module.exports = router;