//  ============================================================================================================
//  Listings routes
//  ============================================================================================================

const express = require("express");
const CheckAuth = require('../middleware/middleware-auth')
const ListingsController = require("../controller/controller-listings")
const router = express.Router()

router.get('/:id', CheckAuth, ListingsController.getListings)

module.exports = router;