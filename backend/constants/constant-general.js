const multer = require("multer");

const SECRET = 'codCKmg80MRf32HwZjlrCGLU7VLd19fw71a8TLhTVuCQrYd7rTKcA67HhPp7KDo';

const MIME_TYPE_MAP = {
  "image/png": "png",
  "image/jpeg": "jpg",
  "image/jpg": "jpg"
};

const ROOT = 'http' + '://' + 'localhost:3001';

const STORAGE = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error("Invalid mime type");
    if (isValid) {
      error = null;
    }
    cb(error, "backend/public");
  },
  filename: (req, file, cb) => {
    const name = file.originalname
      .toLowerCase()
      .split(" ")
      .join("-");
    const ext = MIME_TYPE_MAP[file.mimetype];
    cb(null, Date.now() + "." + ext);
  }
});


module.exports = {
    SECRET,
    MIME_TYPE_MAP,
    STORAGE,
    ROOT
}