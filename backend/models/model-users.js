//  ============================================================================================================
//  User model
//  ============================================================================================================
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator')

// Create our blueprint
const userSchema = mongoose.Schema({
    email: { 
        type: String, 
        required: true, 
        unique: true 
    },
    password: {
        type: String,
        required: true
    },
    createdOn: {
        type: String,
    },
    firstLogin: {
        type: Boolean,
        default: true
    },
    username: {
        type: String,
        unique: true 
    },
    cover: {
        type: String,
    },
    profile: {
        type: String,
    },
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
});

userSchema.plugin(uniqueValidator)

// Create the model
module.exports = mongoose.model('User', userSchema);

