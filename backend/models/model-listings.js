//  ============================================================================================================
//  Listing model
//  ============================================================================================================
const mongoose = require('mongoose');

const timestamps = require('mongoose-timestamp-date-unix');

const listingSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    isDraft: {
        type: Boolean,
        required: true,
        default: true
    },
    title: { 
        type: String,  
    },
    description: { 
        type: String, 
    },
    method: { 
        type: String, 
    },
    delivery: { 
        type: String, 
    },
    cost: { 
        type: Number, 
    },
    postage: { 
        type: Number, 
    },
    images: [{
        type: String
    }],
    createdOn: {
        type: String,
    },
    latitude: {
        type: String
    },
    longitude: {
        type: String
    },
    auctionCurrentBid: {
        type: String
    },
    auctionHighestBid: {
        type: String
    },
    auctionStartDate: {
        type: String
    },
    auctionStartTime: {
        type: String
    },
    winningBidder: {
        type: String
    }
});

listingSchema.plugin(timestamps);

module.exports = mongoose.model('Listing', listingSchema);
