//  ============================================================================================================
//  Listing model
//  ============================================================================================================
const mongoose = require('mongoose');

const timestamps = require('mongoose-timestamp-date-unix');

const biddingSchema = mongoose.Schema({
    itemId: {
        type: String,
        required: true
    },
    bids: [
        {
            userId: {type: String},
            bid: {type: String}
        }
    ]
   
});

biddingSchema.plugin(timestamps);

module.exports = mongoose.model('Bidding', biddingSchema);
